MapDeck
=======

> Contact Isaac Wagner <isaacbwagner@gmail.com> with any questions

Structure
---------

    + MapDeck
    -- build.sbt (build script)
    --+ app
    ----+ public ... (compiled client assets)
    ----+ controllers ... (play controllers)
    ----+ views ... (play templates)
    ----+ ... (other java code)
    --+ conf ... (configuration files, routes)
    --+ antlr ... (grammars)
    --+ public ... (static client assets)
    --+ test ... (test suites)
    --+ project ... (sbt configuration)

Building
--------

sbt will automate most tasks, but you will need to take care of a few things first:

1. Download Typesafe Activator (https://typesafe.com/platform/getstarted), unzip it, and make sure 'activator' is in your PATH
2. Install TypeScript (http://www.typescriptlang.org/#Download) and make sure 'tsc' is in your PATH
3. Install JDK 1.8 and set your JAVA_HOME environment variable to the JDK installation folder
4. cd into conf/ and run 'bower install' whenever bower.json changes (this will be added to the sbt build script eventually)

Most of the time you can just call 'activator run' from within the project root directory. This will spin up sbt and Play which 
will watch for file and configuration changes and rebuild components when needed. Compilation errors will be shown in the browser.

Contibuting
-----------

* *Only use spaces -- don't indent with tabs!*
* Check in Unix line endings, even on Windows (https://help.github.com/articles/dealing-with-line-endings)
* Use consistent indentation: Java=4, CSS=4, Scala=2, TypeScript/JavaScript=2, HTML=2
* Line things up if it makes the code look better, don't if it doesn't

IntelliJ IDEA
-------------

Plugins:

* CSS Support
* HTML Tools
* JavaScript Support
* Scala (not bundled)
* Play 2.0 Support (not bundled)

If you generate an IntelliJ IDEA project, it will have the wrong folders marked as source roots (which will break the autocomplete).
For now, you can just fix this manually by opening the target/scala-2.11/src_managed/main directory and making sure none of the
directories inside it are source roots (blue). To unmark a folder, right click on it and select:

    Mark Directory As->Unmark as Sources Root

Then use the same menu to mark target/scala-2.11/src_managed/main as a Generated Sources Root.

Eclipse
-------

Plugins:

* Data Tools Platform Enablement Extender SDK
* Eclipse Web Developer Tools
* JavaScript Development Tools
* Scala IDE for Eclipse
* TypeScript (either the one from Palantir or TypEcs)

Please make sure that you have all of the different language editors (TypeScript, JavaScript, HTML, Java, Scala, etc.) configured to use spaces instead of tabs, as everything will default to tabs.

None of these plugins are actually necessary to be able to open the project, but they do make working with the files more pleasant.

Git
---

Please rebase instead of merging unless you are merging into master from a branch that is finished. In every other case, rebase instead. They both have the
same conflict resolution, but rebasing results in a much cleaner commit tree and, if you rebase your branch against master regularly, it will reduce
the need for manual conflict resolution. You can set git up to automatically rebase (i.e. add the `--rebase` flag) when pulling: 
http://blog.aplikacja.info/2010/11/git-pull-rebase-by-default/