resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

// Play plugin

addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.3.1")

// Web plugins

addSbtPlugin("com.typesafe.sbt" % "sbt-rjs" % "1.0.1")

addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.0.0")

addSbtPlugin("com.typesafe.sbt" % "sbt-mocha" % "1.0.0")

// ANTLR4

resolvers += "simplytyped.com" at "http://simplytyped.com/repo/releases"

addSbtPlugin("com.simplytyped" % "sbt-antlr4" % "0.7.2")
