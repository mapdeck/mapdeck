module.exports = function(config) {
  config.set({
    basePath: "..",
    port: 9876,
    browsers: ["Chrome", "ChromeCanary", "Firefox", "IE"],
    frameworks: ["mocha"],
    autoWatch: true,
    logLevel: config.LOG_ERROR,
    files: [
      "public/components/platform/platform.js",
      "public/components/polymer/polymer.js",
      "public/components/jquery/dist/jquery.js",
      "public/components/chai/chai.js",
      "public/components/sinon/lib/sinon.js",
      { pattern: "public/elements/*.html", included: false },
      { pattern: "target/web/public/main/javascript/*.js", included: false },
      "test/client/*.js"
    ],
    proxies: {
      "/elements/": "http://localhost:9876/base/public/elements/",
      "/javascript/": "http://localhost:9876/base/target/web/public/main/javascript/"
    }
  });
};