# --- !Ups

ALTER TABLE users ALTER COLUMN created_at SET NOT NULL;

ALTER TABLE polygon_sets ALTER COLUMN owner_id SET NOT NULL;
ALTER TABLE polygon_sets ALTER COLUMN ready SET NOT NULL;

# --- !Downs
