# --- !Ups

CREATE TABLE users (
  id            serial    PRIMARY KEY,
  email         varchar   UNIQUE NOT NULL,
  password_hash varchar   NOT NULL,
  created_at    timestamp DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE polygon_sets (
  id        serial  PRIMARY KEY,
  name      varchar NOT NULL,
  owner_id  integer REFERENCES users (id) ON DELETE RESTRICT,
  ready     boolean DEFAULT false,
  
  UNIQUE (name, owner_id)
);

CREATE TABLE polygons (
  id            serial  PRIMARY KEY,
  set_id        integer NOT NULL REFERENCES polygon_sets (id) ON DELETE CASCADE,
  name          varchar NOT NULL,
  geometry_json text    NOT NULL,
  
  UNIQUE (name, set_id)
);

# --- !Downs
