# --- !Ups

CREATE TABLE data_layers (
  id        serial  PRIMARY KEY,
  set_id    integer REFERENCES polygon_sets (id) ON DELETE RESTRICT,
  owner_id  integer REFERENCES users (id) ON DELETE RESTRICT,
  name      varchar NOT NULL,
  
  UNIQUE(name, owner_id)
);

CREATE TABLE data_points (
  layer_id    integer REFERENCES data_layers (id) ON DELETE CASCADE, -- redundant since we can infer this from polygon_id, but good to have an index on it
  polygon_id  integer REFERENCES polygons (id) ON DELETE CASCADE,
  year        integer,
  val         double  NOT NULL,
  
  PRIMARY KEY (layer_id, polygon_id, year)
);

# --- !Downs
