grammar Expression;

expr: ID # Id
    | NUMBER # Number
    | ID '(' arguments? ')' # Call
    | op='-' expr # UnaryOp
    | '(' expr ')' # Paren
    | <assoc=right> l=expr op='^' r=expr # InfixOp
    | l=expr op=('*' | '/' | '%') r=expr # InfixOp
    | l=expr op=('+' | '-') r=expr # InfixOp
    ;

arguments: expr (',' expr)* ;

ID: [a-zA-Z_] [a-zA-Z0-9_]* ;

NUMBER: '0'
      | [1-9] [0-9]*
      | '0'? '.' [0-9]+
      | [1-9] [0-9]* '.' [0-9]+
      ;

WHITESPACE: [ \t\r\n]+ -> skip ;
