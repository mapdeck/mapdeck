describe("md-properties", function() {
  var expect = chai.expect;
  var el;

  before(function(done) {
    Polymer.import(["/elements/md-properties.html"], function() {
      done();
    });
  });

  beforeEach(function(done) {
    el = document.createElement("md-properties");
    document.body.appendChild(el);
    $('<md-property key="foo">bar</md-property>').appendTo(el);
    $('<md-property key="magic">unicorn</md-property>').appendTo(el);
    var domReady = el.domReady;
    el.domReady = function() {
      domReady.call(el);
      done();
    };
  });

  afterEach(function() {
    document.body.removeChild(el);
    el = null;
  });

  it("should immediately set and return properties", function() {
    el.setProperty("foo", "bar");
    expect(el.getProperty("foo")).to.equal("bar");
  });

  it("should watch for DOM additions", function(done) {
    el.watch("flub", function(value) {
      expect(value).to.equal("blub");
      expect(el.getProperty("flub")).to.equal("blub");
      done();
    });

    $('<md-property key="flub">blub</md-property>').appendTo(el);
  });

  it("should watch for DOM deletions", function(done) {
    el.watch("foo", function(value) {
      expect(value).to.equal(null);
      expect(el.getProperty("foo")).to.equal(null);
      done();
    });

    $(el).find('md-property[key="foo"]').remove();
  });
});
