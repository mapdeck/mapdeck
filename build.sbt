import Keys._
import com.typesafe.sbt.web._
import xsbti.{ Maybe, Position, Severity, Problem }
import WebKeys._
import scala.concurrent.{ Future, ExecutionContext, Await }
import scala.concurrent.duration._
import ExecutionContext.Implicits.global
import com.typesafe.sbt.web.incremental
import com.typesafe.sbt.web.incremental._

name := "MapDeck"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, SbtWeb)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  jdbc,
  javaJdbc,
  javaEbean,
  cache,
  javaWs,
  anorm,
  filters
)

libraryDependencies ++= Seq(
  "org.antlr" % "antlr4-runtime" % "4.3",
  "de.micromata.jak" % "JavaAPIforKml" % "2.2.1",
  "org.springframework.security" % "spring-security-crypto" % "3.2.4.RELEASE"
)

sourceDirectory in Test <<= baseDirectory(_ / "test" / "server")

scalaSource in Test <<= baseDirectory(_ / "test" / "server")

javaSource in Test <<= baseDirectory(_ / "test" / "server")

EclipseKeys.projectFlavor := EclipseProjectFlavor.Scala

// ANTLR4

antlr4Settings

antlr4PackageName in Antlr4 := Some("edu.ithaca.mapdeck.expression.gen")

antlr4GenListener in Antlr4 := true

antlr4GenVisitor in Antlr4 := true

sourceDirectory in Antlr4 := baseDirectory.value / "antlr"

// TypeScript

val typeScriptTask = TaskKey[Seq[File]]("typescript", "Compile TypeScript files")

typeScriptTask := {
  class TypeScriptLogger(file: File) extends ProcessLogger {
    val problems = collection.mutable.Queue[Problem]()
    def info(s: => String): Unit = ()
    def buffer[T](f: => T): T = f
    def error(s: => String) {
      val r = """^(.+?)\(([0-9]+),([0-9]+)\): (error) .+?: (.+)$""".r
      r.findFirstMatchIn(s) match {
        case Some(r(target, line, ch, t, message)) =>
          val severity = t match {
            case "error" => Severity.Error
            case "warning" => Severity.Warn //Not sure if tsc even prints this to stderr
            case _ => Severity.Info
          }
          val lineContent = IO.readLines(new File(target))(line.toInt - 1)
          problems += new LineBasedProblem(message, severity, line.toInt, ch.toInt - 1, lineContent, new File(target))
        case None => ()
      }
    }
  }
  val sourceDir = (sourceDirectory in Assets).value
  val outDir = (resourceManaged in Assets).value
  val sources = sourceDir ** ("*.ts" - "*.d.ts")
  val results = incremental.syncIncremental((streams in Assets).value.cacheDirectory, sources.get) {
    modifiedSources =>
      val s = if (modifiedSources.length > 1) "s" else ""
      if (modifiedSources.length > 0) streams.value.log.log(Level.Info, s"Compiling ${modifiedSources.length} TypeScript source$s to ${(public in Assets).value}...")
      val targets = modifiedSources pair relativeTo(sourceDir) map {
        case (file, relative) =>
          val js = """\.ts$""".r.replaceAllIn(relative, ".js")
          (file, outDir / js, new File(relative))
      }
      val opFutures = targets map {
        case (ts, js, rel) =>
          Future {
            val logger = new TypeScriptLogger(ts)
            val path = Seq("tsc", "--target", "ES5", "--sourcemap", "--sourceRoot", "/" + rel.getParent, "--out", js.toString, ts.toString)
            //println(path)
            val builder: ProcessBuilder = path
            val result = if (builder ! logger == 0) OpSuccess(Set(ts), Set(js, new File(js + ".map"))) else OpFailure
            (logger, ts, result)
          }
      }
      val ops = Await.result(Future.sequence(opFutures), 1 minute)
      val resultMap = ops.foldLeft(Map[File, OpResult]()) {
        case (map, (logger, file, result)) => map + (file -> result)
      }
      (resultMap, ops flatMap (_._1.problems))
  }
  val (files, problems) = results
  CompileProblems.report((reporter in Assets).value, problems)
  files.toSeq
}

//excludeFilter in Assets := "*.ts" //Don't copy TypeScript files to public/

sourceDirectory in Assets := (sourceDirectory in Compile).value / "public"

sourceDirectory in TestAssets := (sourceDirectory in Compile).value / "public"

EclipseKeys.eclipseOutput := Some("bin")

managedResourceDirectories in Assets <+= resourceManaged in Assets

resourceGenerators in Assets <+= typeScriptTask
