///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/data.d.ts" />

interface AggregationFunction {
  (values: Data.Point[]): number;
}

interface FunctionMap {
  [index: string]: AggregationFunction;
}

(function() {
  var functions: FunctionMap = {
    "Change": points => points[points.length - 1].value - points[0].value,
    "Average": points => points.map(p => p.value).reduce((sum, n) => sum + n, 0) / points.length
  };

  Polymer("md-aggregation-functions", {
    functionNames: () => Object.keys(functions),
    getFunction: name => functions[name]
  });
})();