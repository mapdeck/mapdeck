///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/data.d.ts" />
///<reference path="../../types/d3.d.ts" />
///<reference path="../../types/common.d.ts" />
///<reference path="../../types/geojson.d.ts" />

declare var omnivore: any;
declare var L: any;
declare var toGeoJSON: { kml: Function };

interface ValuePair {
  polygon: string;
  value: number
}

(function() {
  var ALPHA = 0.0; //0.15

  var STYLE = {
    NO_SELECTION: { fillOpacity: .8, opacity: 1, color: "white", weight: 2 },
    NOT_SELECTED: { fillOpacity: .5, opacity: 1, color: "white", weight: 2 },
    SELECTED: { fillOpacity: .9, opacity: 1, color: "black", weight: 4 }
  };

  function rgb(r, g, b) {
    return "rgb(" + Math.round(r) + "," + Math.round(g) + "," + Math.round(b) + ")";
  }

  var isCtrlPressed = false;

  document.addEventListener("keydown", function(e: KeyboardEvent) {
    if (e.keyCode == 17) isCtrlPressed = true;
  });

  document.addEventListener("keyup", function(e: KeyboardEvent) {
    if (e.keyCode == 17) isCtrlPressed = false;
  });

  window.addEventListener("blur", function() {
    isCtrlPressed = false;
  });

  Polymer("md-leaflet-map", {
    ready: function() {
      this._selection = [];
      this.addEventListener("resize", function(e) {
        if (this._map) this._map.invalidateSize();
        e.stopPropagation();
      });
    },
    domReady: function() {
      this._loadMap();
    },
    _loadMap: function() {
      var layers = {
        "Street": L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"),
        "Landscape": L.tileLayer("http://{s}.tile.thunderforest.com/landscape/{z}/{x}/{y}.png"),
        "Satellite": L.tileLayer("http://otile1.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpg")
      };

      //TODO: don't hardcode layers.Street
      this._map = L.map(this.$.map, { layers: [layers.Street], attributionControl: false, inertia: false }).setView([39.8282, -98.5795], 5);
      L.control.layers(layers, {}, { position: "topleft" }).addTo(this._map);

      this._map.on("click", (e: KeyboardEvent) => this.clearSelection());

      this._map.on("move", (e) => {
        this.fire("camera", { center: this._map.getCenter(), zoom: this._map.getZoom() });
      });

      this._map.on("zoomstart", (e) => {
        this.fire("camera", { center: this._map.getCenter(), zoom: e.animateToZoom });
      });
    },
    setCamera: function(center, zoom) {
      if (this._map) {
        var currentCenter = this._map.getCenter();
        var currentZoom = this._map.getZoom();
        if (center.lat !== currentCenter.lat || center.lng !== currentCenter.lng || zoom !== currentZoom) {
          this._map.setView(center, zoom, { pan: { animate: false }, zoom: { animate: false } });
        }
    }
  },
    _setPolygonValue: function(polygon: string, value: number) {
      function munge(x: number) {
        //munge() expects value between 0 and 1
        x = Math.pow(x, 1 / 3);
        x *= 1 - ALPHA;
        x += ALPHA;
        return x;
      }

      var poly = this._polygons[polygon];

      //TODO: pick from different scales including linear ones
      var scale = d3.scale.quantile().domain([-1, 0, 1]).range(["#a50026", "#d73027", "#f46d43", "#fdae61", "#fee090", "#ffffbf", "#e0f3f8", "#abd9e9", "#74add1", "#4575b4", "#313695"]);

      if (value < 0) {
        poly.setStyle({ fillColor: scale(value) });
      } else {
        poly.setStyle({ fillColor: scale(value) });
      }
    },
    _refreshData: function() {
      if (this._data && this._polygons) {
        var extent = d3.extent(this._data, (d: ValuePair) => d.value);
        var min = extent[0];
        var max = extent[1];

        this._data.forEach((pair) => {
          if (pair.value > 0) this._setPolygonValue(pair.polygon, pair.value / max);
          else this._setPolygonValue(pair.polygon, - pair.value / min);
        });
      }
    },
    setPolygons: function(polygons: GeoJSON.Feature[]) {
      if (this._polygons) {
        for (var i in this._polygons) {
          this._map.removeLayer(this._polygons[i]);
        }
      }

      this._polygons = {};

      polygons.forEach((polygon) => {
        var poly;

        if (polygon.geometry.type === "MultiPolygon") {
          poly = L.multiPolygon(polygon.geometry.coordinates);
        }

        poly.addTo(this._map);

        poly.on("click", () => {
          this.selectPolygon(polygon.properties.id, isCtrlPressed);
        });

        poly.setStyle(STYLE.NO_SELECTION);
        this._polygons[polygon.properties.id] = poly;
        poly.addTo(this._map);
      });

      this._refreshData();
    },
    setData: function(data: ValuePair[]) {
      this._data = data;
      this._refreshData();
    },
    clearSelection: function() {
      for (var name in this._polygons) this._polygons[name].setStyle(STYLE.NO_SELECTION)
      this._selection = [];
      this.fire("selection", this._selection);
    },
    selectPolygon: function(name: string, group: boolean = false) {
      assert(name in this._polygons);

      var wasSelection = this._selection.length > 0;

      if (!group && wasSelection) {
        this._selection.forEach(name => this._polygons[name].setStyle(STYLE.NOT_SELECTED));
        this._selection = [];
      } else if (!wasSelection) {
        for (var n in this._polygons) this._polygons[n].setStyle(STYLE.NOT_SELECTED);
      }

      this._selection.push(name);

      var poly = this._polygons[name];
      poly.bringToFront();
      poly.setStyle(STYLE.SELECTED);

      this.fire("selection", this._selection);
    }
  });
})();