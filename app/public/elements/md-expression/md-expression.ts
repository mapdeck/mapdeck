///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/common.d.ts" />

(function() {
  Polymer("md-expression", {
    ready: function() {
      this._empty = true;
      this._last = "";
    },
    domReady: function() {
      this.$.edit.spellcheck = false;
      this.$.edit.textContent = "Expression... (e.g. a+b)";

      this.$.edit.addEventListener("keydown", (e) => {
        if (e.keyCode == 13) {
          e.preventDefault();
          this.$.edit.blur(); //TODO: might be an issue with cross-browser stuff
          this.$.edit.textContent = this.$.edit.textContent.replace("\n", "");
          this._commit();
        }
      });

      //TODO: this event doesn't work on IE
      this.$.edit.addEventListener("input", (e) => {
        Array.prototype.forEach.call(this.$.edit.children, el => el.classList.remove("error"));
      });
    },
    setLayers: function(layers: string[]) {
      this._layers = layers;
      this.$.layers.innerHTML = "";
      layers.forEach(layer => {
        var li = document.createElement("li");
        li.textContent = layer;
        this.$.layers.appendChild(li);
      });
    },
    _onFocus: function() {
      if (this._empty) {
        this.$.edit.textContent = "";
        this.$.edit.classList.remove("empty");
      }

      this.$.choices.classList.remove("hidden");
    },
    _onBlur: function() {
      setTimeout(() => {
        if (!this._clickedLayer)
          this._commit();
        else
          this._clickedLayer = false;

        this.$.choices.classList.add("hidden");
      }, 0);
    },
    _commit: function() {
      var text = this.$.edit.textContent;

      if (text === "") {
        this._empty = true;
        this.$.edit.textContent = "Expression... (e.g. a+b)";
        this.$.edit.classList.add("empty");
        this.$.edit.classList.remove("error");
      } else if (text !== this._last) {
        this._empty = false;
        this._last = text;

        this.$.ajax.Expression.inspect(text)((status, result) => {
          var textNodes = Array.prototype.filter.call(this.$.edit.childNodes, node => node.nodeType === 3);
          textNodes.forEach(wrapInSpan);
          Array.prototype.forEach.call(this.$.edit.children, el => el.classList.remove("error"));
          
          result.variables.forEach(variable => {
            if(this._layers.indexOf(variable) === -1) {
              result.errors.push({ message: "unknown layer " + variable, start: 0, length: this.$.edit.textContent.length });
            }
          });

          result.errors.forEach(err => addClass("error", this.$.edit, err.start, err.start + err.length));

          if (!result.errors.length) {
            this.fire("change", { text: this.$.edit.textContent });
          }
        });
      }
    }
  });

  function addClass(className: string, el: HTMLElement, from: number, to: number) {
    split(el, from);
    split(el, to);

    var i = 0;

    Array.prototype.forEach.call(el.children, child => {
      if (i >= from && i < to) {
        child.classList.add(className);
      } else if (i < to) {
        i += child.textContent.length;
      }
    });
  }

  function wrapInSpan(node) {
    var span = document.createElement("span");
    node.parentNode.appendChild(span);
    span.appendChild(node);
    return span;
  }

  function split(el: HTMLElement, index: number): void {
    var i = 0;
    Array.prototype.slice.call(el.children).forEach(span => {
      if (i + span.textContent.length > index) {
        var text = span.firstChild;
        var newText = text.splitText(index - i);
        var span1 = wrapInSpan(text);
        var span2 = wrapInSpan(newText);
        span1.className = span.className;
        span2.className = span.className;

        el.insertBefore(span1, span);
        el.insertBefore(span2, span);
        el.removeChild(span);
      } else if (i === index) {
      } else {
        i += span.textContent.length;
      }
    });
  }
})();