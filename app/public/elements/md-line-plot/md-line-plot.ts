///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/d3.d.ts" />
///<reference path="../../types/data.d.ts" />

interface IPaletteGenerator {
  generate(n: number, check?: (any) => boolean, forceMode?: boolean, quality?: number, ultra?: boolean);
}

declare var paletteGenerator: IPaletteGenerator;

(function() {
  var SNAP_MOUSE_DISTANCE = 10; //Distance to snap line that it will be treated as mouseover

  function offset(el) {
    var x = 0, y = 0;
    do {
      x += el.offsetLeft;
      y += el.offsetTop;
    } while (el = el.offsetParent);
    return { x: x, y: y };
  }

  var margins = { left: 0, bottom: 25, top: 0, right: 0 };

  function dateToNumber(date: Date): number {
    return date.getFullYear();
  }

  function simpleScale(domain, range, margin) {
    var scale = d3.scale.linear().domain(domain).range(range);
    var dir = (range[0] < range[1]) ? 1 : -1;
    var real0 = scale.invert(scale(domain[0]) - margin * dir);
    var real1 = scale.invert(scale(domain[1]) + margin * dir);

    return scale.domain([real0, real1]);
  }

  function range(from: number, to: number): number[] {
    var nums = new Array(to - from + 1);
    for (var i = 0; from <= to; from += 1, i++) nums[i] = from;
    return nums;
  }

  Polymer("md-line-plot", {
    ready: function() {
      this.addEventListener("resize", this._resize.bind(this));
    },
    domReady: function() {
      var chart = d3.select(this.$.svg).append("g")
        .attr("class", "chart")
        .attr("transform", "translate(" + margins.left + "," + margins.top + ")");

      chart.append("rect")
        .attr("class", "back")
        .attr("fill", "#FFF")
        .attr("x", 0).attr("y", 0);

      chart.append("g").attr("class", "snaps");
      chart.append("g").attr("class", "lines");
      chart.append("g").attr("class", "ghosts");
      chart.append("g").attr("class", "time axis");
    },
    _things: function() {
      //TODO: this isn't actually used outside of _resize() anymore, so merge them
      var data: Data.Polygon[] = this._data;

      var width = this.clientWidth - margins.left - margins.right;
      var height = this.clientHeight - margins.top - margins.bottom;

      var timeExtent = d3.extent(data.map(polygon => {
        return polygon.data.map(point => dateToNumber(point.time));
      }).reduce((a, b) => a.concat(b)));

      var valueExtent = d3.extent(data.map(polygon => {
        return polygon.data.map(point => point.value);
      }).reduce((a, b) => a.concat(b)));

      var timeScale = simpleScale(timeExtent, [0, width], 35);
      var valueScale = simpleScale(valueExtent, [height, 0], 35);

      var timeAxisFn = d3.svg.axis().scale(timeScale).orient("bottom")
        .outerTickSize(0).innerTickSize(4)
        .tickValues(range(timeExtent[0], timeExtent[1]))
        .tickFormat(n => n.toFixed());

      var timeAxis = function(selection) {
        var axis = selection.call(timeAxisFn);

        axis.selectAll("g.tick > text")
          .attr("y", margins.bottom / 2)
          .attr("dy", "0.4em");

        axis.insert("rect", ":first-child")
          .attr("x", 0)
          .attr("y", 0)
          .attr("width", width)
          .attr("height", margins.bottom)
          .attr("fill", "#DDD");

        return axis;
      }

      var valueAxis = function(selection) {
        var axisFn = d3.svg.axis().scale(valueScale).orient("left")
          .outerTickSize(0);
        var axis = selection.call(axisFn);

        axis.selectAll("text").attr("x", 4);

        return axis;
      }

      var genLine = d3.svg.line()
        .x(point => timeScale(dateToNumber(point.time)))
        .y(point => valueScale(point.value));

      function dot(s) {
        return s.attr("r", 5)
          .attr("cx", d => timeScale(dateToNumber(d.time)))
          .attr("cy", d => valueScale(d.value))
          .attr("stroke-width", 2)
          .attr("stroke", "white")
      }

      return {
        fullWidth: this.clientWidth,
        fullHeight: this.clientHeight,
        width: width,
        height: height,
        timeExtent: timeExtent,
        valueExtent: valueExtent,
        timeScale: timeScale,
        valueScale: valueScale,
        timeAxis: timeAxis,
        valueAxis: valueAxis,
        genLine: genLine,
        ticks: range(timeExtent[0], timeExtent[1]),
        dot: dot
      };
    },
    _resize: function(e) {
      e.stopPropagation();

      if (!this._data || this._data.length === 0) return;

      var t = this._things();

      var chart = d3.select(this.$.svg)
        .attr("width", t.fullWidth)
        .attr("height", t.fullHeight)
        .select("g.chart");

      var back = chart.select(".back").attr("width", t.width).attr("height", t.height);

      //back.style("cursor", (hi !== undefined) ? "pointer" : null);

      //Ehhhh... some of this should be in setData
      var snaps = chart.select("g.snaps").selectAll("line.snap").data(t.ticks)
        .attr("class", "snap")
        .attr("y1", 0)
        .attr("y2", t.height)
        .attr("x1", d => t.timeScale(d))
        .attr("x2", d => t.timeScale(d));

      var values = chart.selectAll("line.value").data([0]);
      values.attr("class", "value")
        .attr("x1", 0)
        .attr("x2", t.width)
        .attr("y1", d => t.valueScale(d))
        .attr("y2", d => t.valueScale(d));

      chart.select("g.time.axis")
        .attr("transform", "translate(0," + t.height + ")")
        .call(t.timeAxis);

      chart.selectAll("path.line").attr("d", poly => t.genLine(poly.data));

      chart.selectAll("g.dots > circle.dot").call(t.dot);
    },
    setData: function(data: Data.Polygon[]) {
      this._data = data;

      if (data.length == 0) {
        this.$.selectionMessage.style.display = "block";
        this.$.svg.style.display = "none";
      } else {
        this.$.selectionMessage.style.display = "none";
        this.$.svg.style.display = "block";

        var t = this._things();

        var chart = d3.select(this.$.svg)
          .attr("width", t.fullWidth)
          .attr("height", t.fullHeight)
          .select("g.chart");

        var snaps = chart.select("g.snaps").selectAll("line.snap").data(t.ticks);
        snaps.enter().insert("line", "g").attr("class", "snap");
        snaps.exit().remove();
        snaps.attr("y1", 0)
          .attr("y2", t.height)
          .attr("x1", d => t.timeScale(d))
          .attr("x2", d => t.timeScale(d))
          .attr("stroke", "#DDD")
          .attr("stroke-width", 1);

        var values = chart.selectAll("line.value").data([0], d => d);
        values.transition()
          .duration(500)
          .attr("y1", d => t.valueScale(d))
          .attr("y2", d => t.valueScale(d));
        values.enter().append("line").attr("class", "value")
          .attr("y1", d => t.valueScale(d))
          .attr("y2", d => t.valueScale(d));
        values.attr("x1", 0)
          .attr("x2", t.width)
          .attr("stroke", "#555")
          .attr("stroke-width", 1)
          .attr("stroke-dasharray", "1, 5");

        chart.select("g.time.axis")
          .attr("transform", "translate(0," + t.height + ")")
          .call(t.timeAxis);

        var lines = chart.select("g.lines").selectAll("path").data(data, poly => poly.name);

        lines
          .attr("stroke", d => d.color)
          .transition()
          .duration(500)
          .attr("d", poly => t.genLine(poly.data));

        var newLine = lines.enter().append("path")
          .attr("class", "line normal")
          .attr("stroke-width", 1)
          .attr("fill", "none")
          .attr("stroke", d => d.color);

        //TODO: don't do transitions if the change in line number is significant (e.g. > 10)
        //TODO: also, this _oldT thing is terrible
        if (this._oldT) {
          newLine
            .attr("d", poly => this._oldT.genLine(poly.data))
            .style("opacity", 0)
            .transition()
            .duration(500)
            .attr("d", poly => t.genLine(poly.data))
            .style("opacity", 1);
        } else {
          newLine.attr("d", poly => t.genLine(poly.data));
        }

        lines.exit().transition().duration(250).style("opacity", 0).remove();

        var ghosts = chart.select("g.ghosts").selectAll("path").data(data, poly => poly.name);
        ghosts.enter().append("path")
          .attr("class", "line.ghost")
          .attr("stroke-width", 7)
          .attr("stroke", "#000")
          .attr("fill", "none")
          .attr("opacity", "0");
        ghosts.attr("d", poly => t.genLine(poly.data));
        ghosts.exit().remove();

        var dotGroups = chart.selectAll("g.dots").data(data, poly => poly.name);
        dotGroups.enter().append("g")
          .attr("class", "dots")
          .attr("fill", d => d.color)
          .attr("visibility", "hidden");
        dotGroups.attr("fill", d => d.color);
        dotGroups.exit().remove();

        var dots = dotGroups.selectAll("circle.dot").data(poly => poly.data);
        dots.enter().append("circle")
          .attr("class", "dot")
          .attr("r", 5)
          .attr("cx", d => t.timeScale(dateToNumber(d.time)))
          .attr("cy", d => t.valueScale(d.value))
          .attr("stroke-width", 2)
          .attr("stroke", "white");
        dots.exit().remove();
        dots.attr("cx", d => t.timeScale(dateToNumber(d.time)))
          .transition()
          .duration(500)
          .attr("cy", d => t.valueScale(d.value));

        function highlightSnap(y?: number) {
          if (y !== undefined) snaps.filter(d  => d === y).attr("stroke", "#888");
          else snaps.attr("stroke", "#DDD");
        }

        var back = chart.select(".back").attr("width", t.width).attr("height", t.height);

        back.on("mousemove", function() {
          var x = d3.mouse(this)[0] - margins.left;
          var inverted = t.timeScale.invert(x);
          var year = Math.round(inverted);
          var distance = Math.abs(x - t.timeScale(year));

          if (distance < SNAP_MOUSE_DISTANCE) {
            highlightSnap(year);
            chart.style("cursor", "pointer");
          } else {
            highlightSnap();
            chart.style("cursor", null);
          }
        });

        back.on("mouseout", function() {
          highlightSnap();
          chart.style("cursor", null);
        });

        chart.select("g.ghosts").selectAll("path").on("mouseover", function(d, i) {
          chart.select("g.lines").selectAll("path").attr("opacity", (_, j) => (j === i) ? 1 : 0.6); //TODO: magic number
          chart.select("g.lines").selectAll("path").attr("stroke-width", (_, j) => (j === i) ? 3 : 1); //TODO: magic number

          chart.selectAll("g.dots").filter((_, j) => j === i).attr("visibility", "visible");

          var labels = chart.selectAll("g.label").data(d.data);
          labels.enter().append("g")
            .attr("class", "label")
            .attr("transform", point => "translate(" + t.timeScale(dateToNumber(point.time)) + ", " + Math.max(t.valueScale(point.value, 36)) + ")")
            .append("text")
            .text(point => Math.round(point.value * 10) / 10)
            .attr("text-anchor", "middle")
            .attr("dy", "-1.2em")
            .attr("font-size", 12)
            .attr("font-weight", "bold")
            .attr("fill", d.color);

          labels.each(function() {
            var label = d3.select(this);
            var text = <SVGTextElement> label.select("text").node();
            var bbox = text.getBBox();

            label.insert("rect", "text")
              .attr("x", bbox.x - 5)
              .attr("y", bbox.y - 2)
              .attr("stroke", d.color)
              .attr("stroke-width", 2)
              .attr("fill", "#FFF")
              .attr("rx", 3)
              .attr("width", bbox.width + 5 * 2)
              .attr("height", bbox.height + 2 * 2);
          });
        });

        chart.select("g.ghosts").selectAll("path").on("mouseout", function(d, i) {
          chart.selectAll("g.label").remove();
          chart.select("g.lines").selectAll("path").attr("opacity", 1);
          chart.select("g.lines").selectAll("path").attr("stroke-width", 1);
          chart.selectAll("g.dots").attr("visibility", "hidden");
        });
        
        this._oldT = t;
      }
    }
  });
})();