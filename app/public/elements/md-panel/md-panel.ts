///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/data.d.ts" />
///<reference path="../../types/d3.d.ts" />
///<reference path="../../types/geojson.d.ts" />

declare var STATES: any;

(function() {
  //stackoverflow.com/questions/5755312/getting-mouse-position-relative-to-content-area-of-an-element
  function offset(e) {
    var x = 0, y = 0;
    do {
      x += e.offsetLeft;
      y += e.offsetTop;
    } while (e = e.offsetParent);
    return { x: x, y: y };
  }

  //Should do some sort of k-means clustering like http://tools.medialab.sciences-po.fr/iwanthue/
  function generateColors(n: number) {
    var colors = new Array(n);
    for (var i = 0; i < n; i++)
      colors[i] = d3.rgb(Math.round(Math.random() * 255), Math.round(Math.random() * 255), Math.round(Math.random() * 255)).toString();
    return colors;
  }

  Polymer("md-panel", {
    domReady: function() {
      var split = this.shadowRoot.getElementById("splitter");
      var map = this.shadowRoot.getElementById("map");
      var plot = this.shadowRoot.getElementById("plot");
      var lineShow = this.shadowRoot.getElementById("lineShow");

      this._setSplitter(this.clientHeight / 2);

      this._selection = [];

      this.$.map.addEventListener("camera", (e) => {
        this.fire("linkState", { camera: e.detail });
      });

      lineShow.addEventListener('click', () => {
        if (lineShow.className === "lineShow active") {
          split.classList.remove("hidden");
          this.$.map.style.height = (this.clientHeight / 2) + "px";
          this.$.splitter.style.top = (this.clientHeight / 2) + "px";
          this.$.plot.style.top = (this.clientHeight / 2) + "px";
          plot.classList.remove("hidden");
          plot.fire("resize");
          map.fire("resize");
          lineShow.classList.remove("active");
        }
      });

      split.addEventListener("mousedown", e => {
        e.preventDefault();
        this._mouseDown = true;
        document.body.style.setProperty("cursor", "-webkit-grabbing", "important"); //TODO: use class and body.grabbing * selector
      });

      window.addEventListener("mouseup", e => {
        if (this._mouseDown) {
          this._mouseDown = false;
          document.body.style.removeProperty("cursor");
          var y = e.clientY - offset(this).y;
          if (y > this.clientHeight - 50) {
            plot.classList.add("hide");
            split.classList.add("hide");
            lineShow.classList.add("active");
          }
        }
      });

      window.addEventListener("blur", e => {
        this._mouseDown = false;
        document.body.style.removeProperty("cursor");
      });

      window.addEventListener("mousemove", e => {
        if (this._mouseDown) {
          var y = e.clientY - offset(this).y;
          if (y < 50) y = 50;
          else if (y > this.clientHeight - 50) y = this.clientHeight;
          this._setSplitter(y);
        }
      });

      this.$.map.addEventListener("selection", (e) => {
        this._selection = e.detail;
        this._refreshData(this._data);
      });

      this.$.fnSelect.value = "Average"; //TODO nope
      this._aggregationChanged(); //TODO

      this._lastHeight = this.clientHeight;

      this.addEventListener('resize', () => {
        var fraction = parseInt(this.$.splitter.style.top) / this._lastHeight; //TODO maybe store the split position somewhere else
        this._lastHeight = this.clientHeight;

        this._setSplitter(this.clientHeight * fraction);

        map.fire("resize");
        plot.fire("resize");
      });

    },
    polygonsChanged: function(old, value) {
      this.$.ajax.Data.layers(value)((status, result: String[]) => {
        this.$.expression.setLayers(result);
      });

      this.$.ajax.Polygons.showJson(value)((status, result: GeoJSON.FeatureCollection) => {
        this.$.map.setPolygons(result.features);
      });
    },
    setLinkState: function(state) {
      this.$.map.setCamera(state.camera.center, state.camera.zoom);
    },
    _aggregationChanged: function() {
      this._aggregate = this.$.aggregators.getFunction(this.$.fnSelect.value);

      this._refreshData(this._data);
    },
    _setSplitter: function(y: number) {
      y = Math.round(y);

      var offset = Math.floor(this.$.splitter.clientHeight / 2);
      this.$.map.style.height = (y - offset) + "px";
      this.$.splitter.style.top = y + "px";
      this.$.plot.style.top = (y + offset) + "px";
      this.$.map.fire("resize");
      this.$.plot.fire("resize");
    },
    _toggleMenu: function() {
      this.$.menu.classList.toggle("panel-menu-closed");
      this.$.gear.classList.remove("open");
      this.$.gearMenu.classList.add("hidden");
    },
    _toggleGearMenu: function() {
      this.$.gearMenu.classList.toggle("hidden");
      this.$.gear.classList.toggle("open");
    },
    _expressionChanged: function(e) {
      var expr = e.detail.text;
      this.$.ajax.Expression.evaluate(this.polygons, expr)((status, result) => {
        this._data = result;
        this._refreshData(this._data);
      });
    },
    _refreshData: function() {
      var data = this._data;
      if (data !== this._previousData) {
        if (data) {
          var aggregatedData = Object.keys(data).map((id) => {
            var myData = data[id];
            return { polygon: id, value: this._aggregate(myData) };
          });

          this.$.map.setData(aggregatedData);

          if (!this._colors || Object.keys(data).length !== this._colors.length)
            this._colors = generateColors(Object.keys(data).length);
        }
      } else {
        this.$.plot.setData([]);
      }

      if (this._selection.length > 0) {
        var polygons = [];
        Object.keys(data).forEach((id, i) => {
          if (this._selection.indexOf(parseInt(id)) > -1) {
            var data = this._data[id];
            polygons.push({ name: id, color: this._colors[i], data: data.map(pair => ({ time: new Date(parseInt(pair.year), 0), value: pair.value })) });
          }
        });

        this.$.plot.setData(polygons);
      }

      this._previousData = data; //not good at all, bad bad bad
    }
  });
})();