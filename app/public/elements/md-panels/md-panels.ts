///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/common.d.ts" />
///<reference path="../../types/geojson.d.ts" />

declare function Spinner(opts): void;

Polymer("md-panels", {
  ready: function() {
    this._panels = [];
  },
  domReady: function() {
    this.$.spinner.classList.add("hidden");

    var opts = {
      lines: 11, // The number of lines to draw
      length: 7, // The length of each line
      width: 2, // The line thickness
      radius: 8, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb or array of colors
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      shadow: false, // Whether to render a shadow
      hwaccel: false, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '50%', // Top position relative to parent
      left: '50%' // Left position relative to parent
    };

    var spinner = new Spinner(opts).spin();
    this.$.spinner.appendChild(spinner.el);

    window.addEventListener("resize", () => {
      this.fire("resize");
    });

    this.addEventListener("resize", () => {
      this._panels.forEach(panel => panel.fire("resize"));
    });
  },
  _resizePanels: function() {
    var n = this._panels.length;
    this._panels.forEach((el, i) => {
      el.style.left = (i / n * 100) + "%";
      el.style.width = (100 / n) + "%";
      el.fire("resize");
    });
  },
  addPanel: function(polygonSet: number) {
    var panel: any = document.createElement("md-panel");
    panel.setAttribute("polygons", polygonSet.toString());
    this.$.container.appendChild(panel);
    this._panels.push(panel);

    panel.addEventListener("linkState", (e) => {
      var state = e.detail;
      this._panels.forEach((p) => {
        if (p !== panel) p.setLinkState(state);
      });
    });

    this._resizePanels();

    this.$.spinner.classList.remove("hidden");

    var n = this._panels.length;
    this.$.spinner.style.left = ((n - 1) / n * 100) + "%";
    this.$.spinner.style.width = (100 / n) + "%";

    setTimeout(() => {
      panel.style.opacity = "1.0";
      this.$.spinner.classList.add("hidden");
    }, 50);

    var command = {
      name: "add panel",
      undo: () => {
        //panel.remove();
        this._panels.splice(this._panels.indexOf(panel), 1);
        this._resizePanels();
      },
      redo: () => {
        alert("Sorry, no take backs.");
      },
    };

    this.$.commands.recordCommand(command, false);
  },
  removePanel: function(panel: Element) {
  }
});
