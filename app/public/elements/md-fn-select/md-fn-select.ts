///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/common.d.ts" />

(function() {
  var prototype = Object.create(HTMLSelectElement.prototype);

  prototype.attachedCallback = function() {
    var functions: any = document.createElement("md-aggregation-functions");
    functions.functionNames().forEach(name => {
      var option = document.createElement("option");
      option.setAttribute("value", name);
      option.textContent = name;
      this.appendChild(option);
    });

    this.addEventListener("change", () => {
      if(this._properties) this._properties.setProperty("aggregation-function", this.value);
    });

    var value = this.getAttribute("propertiesId");
    if(value) this._propertiesIdChanged(value);
  };

  prototype.attributeChangedCallback = function(attr) {
    if(attr === "propertiesId") {
      var value = this.getAttribute("propertiesId");
      this._propertiesIdChanged(value);
    }
  };

  prototype._propertiesIdChanged = function(value) {
    if(this._properties) {
      this._properties.setProperty("aggregation-function", null);
      delete this._properties;
    }

    var el = findRoot(this).getElementById(value);
    assert(el.localName === "md-properties", "propertiesId should point to <md-properties> element");

    this._properties = el;
    this._properties.setProperty("aggregation-function", this.value);
  }

  function findRoot(el) {
    for(; el.parentNode; el = el.parentNode);
    return el;
  }

  document.registerElement("md-fn-select", {
    prototype: prototype,
    extends: "select"
  });
})();
