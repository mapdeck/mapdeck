///<reference path="../../types/polymer.d.ts" />

interface DataPoint {
  [index: number]: number
}

class Polygon {
  getData(): DataPoint[] {
    return [];
  }
}

Polymer("md-data", {
  domReady: function() {
    function updated(observer, mutations) {
      this.onMutation(this, updated);
      //...
    };

    this.onMutation(this, updated)
  },
  getPolygons: function(): Polygon[] {
    //...
    return [];
  }
});