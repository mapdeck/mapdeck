///<reference path="../../types/polymer.d.ts" />

declare function Spinner(opts): void;

Polymer("md-dashboard", {
  domReady: function() {
    var opts = {
      lines: 9, // The number of lines to draw
      length: 5, // The length of each line
      width: 2, // The line thickness
      radius: 7, // The radius of the inner circle
      corners: 1, // Corner roundness (0..1)
      rotate: 0, // The rotation offset
      direction: 1, // 1: clockwise, -1: counterclockwise
      color: '#000', // #rgb or #rrggbb or array of colors
      speed: 1, // Rounds per second
      trail: 60, // Afterglow percentage
      hwaccel: true, // Whether to use hardware acceleration
      className: 'spinner', // The CSS class to assign to the spinner
      zIndex: 2e9, // The z-index (defaults to 2000000000)
      top: '50%', // Top position relative to parent
      left: '50%' // Left position relative to parent
    };

    var spinner = new Spinner(opts).spin(this.$.dialogSpinner);
  },
  _addPanel: function() {
    //TODO: loading message for if/when it takes longer to process (now very fast)
    this.$.ajax.Polygons.listJson()((status, result) => {
      if (status === 200) {
        this.$.panelSelect.innerHTML = ""; //TODO: bad
        result.forEach(polygon => {
          var option = document.createElement("option");
          option.value = polygon.id;
          option.text = polygon.name;
          this.$.panelSelect.appendChild(option);
        });
        
        this.$.dialogContainer.classList.remove("hidden");
      } else {
        throw new Error(status);
      }
    });
  },
  _submitPanel: function() {
    this.$.dialogContainer.classList.add("hidden");
    
    var polygonSet = parseInt(this.$.panelSelect.value);
    this.$.panels.addPanel(polygonSet);
  },
  emailChanged: function(old, value) {
  },
  _closeDialog: function() {
    this.$.dialogContainer.classList.add("hidden");
  }
});