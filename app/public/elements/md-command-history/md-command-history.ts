///<reference path="../../types/polymer.d.ts" />
///<reference path="../../types/common.d.ts" />

(function() {
  var index: number = 0;
  var commands: Command[] = [];

  function undo() {
    console.log("undo");
    if(index > 0) {
      commands[index - 1].undo();
      index -= 1;
    }
  }

  function redo() {
    console.log("redo");
    if(index < commands.length) {
      commands[index].redo();
      index += 1;
    }
  }

  //TODO: behave properly in browsers with other key-bindings (e.g. IE)
  //TODO: don't undo repeatedly if Ctrl-Z is held down
  document.addEventListener("keydown", function(e: KeyboardEvent) {
    if(e.keyCode === 90 && e.ctrlKey && !e.shiftKey) {
      e.preventDefault();
      undo();
    } else if (e.keyCode === 90 && e.ctrlKey && e.shiftKey) {
      e.preventDefault();
      redo();
    }
  });

  Polymer("md-command-history", {
    recordCommand: function(command: Command, doNow: boolean = true) {
      if(index < commands.length) commands.splice(index, commands.length);

      commands.push(command);
      index += 1;

      if(doNow) command.redo();
    },
    undo: function() {
      undo();
    },
    redo: function() {
      redo();
    },
    peekUndo: function() {
      if(index === 0) return null;
      return commands[index - 1].name;
    },
    peekRedo: function() {
      if(index === commands.length) return null;
      return commands[index].name;
    }
  });
})();