//Unfortunately there doesn't seem to be a way to not duplicate what's in scripts/data.ts with the current state of the TypeScript compiler
declare module Data {
  export interface Point {
    time: Date;
    value: number;
  }

  export interface Polygon {
    name: string;
    color?: string;
    data: Point[];
  }

  export function getPolygonNames(data: Polygon[]): string[];
  export function dataFromJSON(json: string): Polygon[];
}

declare module Geometry {
  export interface LatLng {
    lat: number;
    lng: number;
  }

  export interface Geometry {
    name: string
    polygons: Polygon[]
  }

  export interface Polygon {
    outer: LatLng[]
    inner: LatLng[][]
  }

  export function latLng(lat: number, lng: number): LatLng;
}