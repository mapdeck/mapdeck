interface Assert {
  (exp: boolean, message?: string): void;
}

declare var assert: Assert;

interface Document {
  registerElement(name: String, config: Object): void
}


interface Command {
  undo: () => void;
  redo: () => void;
  name: string;
}
