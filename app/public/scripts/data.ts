module Data {
  export interface Point {
    time: Date;
    value: number;
  }

  export interface Polygon {
    name: string;
    color: string;
    data: Point[];
  }

  export function getPolygonNames(data: Polygon[]): string[] {
    return data.map(data => data.name);
  }

  export function dataFromJSON(json: string): Polygon[] {
    var raw = JSON.parse(json);
    return raw.map(polygonData => {
      var data = polygonData.map(dataPoint => {
        return { time: new Date(dataPoint.time), value: dataPoint.value };
      });
      return { polygon: polygonData.polygon, data: data };
    });
  }
}

module Geometry {
  export interface LatLng {
    lat: number;
    lng: number;
  }

  export interface Geometry {
    name: string
    polygons: Polygon[]
  }

  export interface Polygon {
    outer: LatLng[]
    inner: LatLng[][]
  }
  
  export function latLng(lat: number, lng: number): LatLng {
    return { lat: lat, lng: lng };
  }
}