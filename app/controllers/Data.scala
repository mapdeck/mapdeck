package controllers

import play.api.mvc.Controller
import web.AuthAction
import play.api.data._
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.db._
import anorm._
import anorm.SqlParser._
import play.api.Play.current
import play.api.libs.json._
import edu.ithaca.mapdeck.util.ImprovedNoise

object Data extends Controller {

  def layers(set: Int) = AuthAction { implicit request =>
    DB.withConnection { implicit connection =>
      val names = SQL"""SELECT name FROM data_layers WHERE set_id = ${set}""".as((for {
        name <- str("name")
      } yield name).*)

      Ok(JsArray(names map (x => JsString(x))))
    }
  };

  case class Generate(name: String, polygons: Int, min: Double, max: Double, scale: Double, change: Double)

  val generateForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "polygons" -> number,
      "min" -> of[Double],
      "max" -> of[Double],
      "scale" -> of[Double],
      "change" -> of[Double]
    )(Generate.apply)(Generate.unapply)
  )

  def generation = AuthAction {
    Ok(views.html.Data.generate.render(generateForm, Seq()))
  }

  //TODO: everything
  def generate = AuthAction { implicit request =>
    val form = generateForm.bindFromRequest
    if (form.hasErrors) {
      BadRequest(views.html.Data.generate.render(form, Seq()))
    } else {
      DB.withConnection { implicit connection =>
        val polygons = SQL"""SELECT id, geometry_json FROM polygons WHERE set_id = ${form.get.polygons}""".as((for {
          id <- int("id")
          geometryStr <- str("geometry_json")
        } yield (id, geometryStr)).*)

        //TODO: transaction, maybe?
        val points = polygons map {
          case (id, geometryStr) =>
            val geometry = Json.parse(geometryStr)
            val point = (geometry \ "coordinates")(0)(0)(0)
            val lat = point(0).as[JsNumber].value.doubleValue
            val long = point(1).as[JsNumber].value.doubleValue

            (id, lat, long)
        }

        val (ids, lats, longs) = points.unzip3
        val maxLat = lats.max
        val minLat = lats.min
        val maxLong = longs.max
        val minLong = longs.min

        val latRange = maxLat - minLat
        val longRange = maxLong - minLong

        val data = points map {
          case (id, lat, long) =>
            val relLat = (lat - minLat) * 255 / latRange * form.get.scale;
            val relLong = (lat - minLong) * 255 / longRange * form.get.scale;
            val values = for (i <- List.range(2001, 2011)) yield i -> ((ImprovedNoise.noise(relLat, relLong, (i - 2001.0) / 10.0 * 255 * form.get.change) + 1) / 2 * (form.get.max - form.get.min) + form.get.min)
            id -> values
        }

        val layerId = SQL"""INSERT INTO data_layers (set_id, owner_id, name) VALUES (${form.get.polygons}, ${request.user.id}, ${form.get.name})""".executeInsert(SqlParser.scalar[Int].singleOpt)

        for ((id, values) <- data; (year, value) <- values)
          SQL"""INSERT INTO data_points (layer_id, polygon_id, year, val) VALUES ($layerId, $id, $year, $value)""".executeInsert(SqlParser.scalar[Int].singleOpt)
      }

      Ok(form.get.toString)
    }
  }
}