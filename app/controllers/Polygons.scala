package controllers

import play.api.data._
import play.api.data.Forms._
import play.api.mvc.Controller
import play.api.mvc.Action
import play.api.Play.current
import play.api.db._
import scala.collection.JavaConversions._
import edu.ithaca.mapdeck.geometry._
import java.util.stream.Collectors
import anorm._
import web.AuthAction
import play.api.libs.json._
import models._

object Polygons extends Controller {
  case class Create(name: String, public: Boolean, uploadType: String)

  val createForm = Form(
    mapping(
      "name" -> nonEmptyText,
      "public" -> boolean,
      "uploadType" -> text
    )(Create.apply)(Create.unapply)
  )

  object UploadType extends Enumeration {
    type UploadType = Value
    val KML = Value
  }

  val uploadTypes = Seq(
    UploadType.KML -> "KML"
  ) map { case (t, s) => t.toString -> s }

  def create = AuthAction {
    Ok(views.html.Polygons.create.render(createForm, uploadTypes))
  }

  def upload = AuthAction(parse.multipartFormData) { implicit request =>
    val form: Form[Create] = if (request.body.files.isEmpty) createForm.bindFromRequest withGlobalError "At least one file must be selected for upload" else createForm.bindFromRequest
    if (form.hasErrors) {
      BadRequest(views.html.Polygons.create.render(form, uploadTypes))
    } else {
      //TODO: should be in an actor
      val files = request.body.files map (filePart => filePart.ref.file)
      val geometry = files flatMap ({ file =>
        val source = new KmlSource(file)
        source.getGeometry().collect(Collectors.toList())
      })

      //TODO: don't parse the files before checking if the set can even be created. Huge waste of resources
      DB.withConnection { implicit connection =>
        val insertResult = SQL("""INSERT INTO polygon_sets (name, owner_id) VALUES ({name}, {owner_id});""")
          .on("name" -> form.get.name, "owner_id" -> request.session.get("id"))
          .executeInsert(SqlParser.scalar[Int].singleOpt)

        insertResult.headOption.fold {
          BadRequest("Couldn't do the thing you wanted. Sorry.")
        } { id =>
          geometry foreach { geom =>
            def coordsAsJson(coords: Seq[LatLng]) =
              new JsArray(coords map (ll => Json.arr(JsNumber(ll.getLat), JsNumber(ll.getLng))))

            def polyAsJson(polygon: edu.ithaca.mapdeck.geometry.Polygon) = {
              val outer: Seq[LatLng] = polygon.getOuter
              val inner: Seq[Seq[LatLng]] = polygon.getInner map asScalaBuffer
              JsArray(Seq(coordsAsJson(outer)) ++ (inner map coordsAsJson))
            }

            val json = Json.obj(
              "type" -> "MultiPolygon",
              "coordinates" -> new JsArray(
                geom.getPolygons map polyAsJson
              )
            )

            try {
              SQL("""INSERT INTO polygons (set_id, name, geometry_json) VALUES ({set_id}, {name}, {geometry_json});""")
                .on("set_id" -> id, "name" -> (geom.metaData.get("STATEFP") + geom.metaData.get("COUNTYFP")),
                  "geometry_json" -> json.toString)
                .executeInsert(SqlParser.scalar[Int].singleOpt)
            } catch {
              case e => println(e)
            }
          }

          Redirect(routes.Polygons.show(id.toInt))
        }
      }
    }
  }

  def show(id: Int) = AuthAction { implicit request =>
    DB.withConnection("default") { implicit connection =>
      PolygonSet(id).fold {
        NotFound("Nope!")
      } { polygonSet =>
        if (polygonSet.ownerId != request.user.id) {
          NotFound("Get outa' here!")
        } else {
          Ok(views.html.Polygons.show.render(polygonSet))
        }
      }
    }
  }

  /**
   * Return a GeoJSON representation of this polygon as a feature
   */
  def showJson(id: Int) = AuthAction { implicit request =>
    DB.withConnection { implicit connection =>
      val result = SQL("""SELECT name, ready, owner_id FROM polygon_sets WHERE id = {id}""").on("id" -> id.toString)()
      result.headOption.fold {
        NotFound("Nope!")
      } { row =>
        val name = row[String]("name")
        val ready = row[Boolean]("ready")
        val owner_id = row[Int]("owner_id")

        //check if it's ready

        //TODO: handle exception if parsing fails
        if (owner_id != request.user.id) {
          NotFound("Nope!")
        } else {
          val polygonsResult = SQL("""SELECT id, name, geometry_json FROM polygons WHERE set_id = {set_id}""").on("set_id" -> id)()

          //Yuck, but it makes a *huge* difference
          val builder: StringBuilder = new scala.collection.mutable.StringBuilder

          val strings = polygonsResult map { row =>
            val geometry = row[String]("geometry_json")

            val properties = Json.obj(
              "name" -> row[String]("name"),
              "id" -> row[Int]("id")
            ).toString

            s"""{"type":"Feature","geometry":$geometry,"properties":$properties}"""
          }

          builder ++= strings.head
          strings.drop(1) foreach { builder ++= "," ++= _ }

          Ok(s"""{"type":"FeatureCollection","features":[${builder.toString}]}""").as("application/json")
        }
      }
    }
  }

  def listJson = Action { implicit request =>
    DB.withConnection { implicit connection =>
      val result = SQL("""SELECT name, id FROM polygon_sets WHERE owner_id = {owner_id}""").on("owner_id" -> request.session.get("id").get)()
      val sets = result map { row =>
        Json.obj(
          "name" -> row[String]("name"),
          "id" -> row[Int]("id")
        )
      }

      Ok(new JsArray(sets))
    }
  }

  def list = Action { implicit request =>
    DB.withConnection { implicit connection =>
      val result = SQL("""SELECT polygon_sets.name, polygon_sets.id, COUNT(polygons.id) AS num
                          FROM polygon_sets LEFT OUTER JOIN polygons ON (polygons.set_id = polygon_sets.id)
                          WHERE polygon_sets.owner_id = {owner_id}
                          GROUP BY (polygon_sets.name, polygon_sets.id)
                          ORDER BY polygon_sets.id
                       """).on("owner_id" -> request.session.get("id").get)()
      val sets = result map { row =>
        (row[Int]("id"), row[String]("name"), row[BigInt]("num").intValue)
      }

      Ok(views.html.Polygons.list.render(sets.toSeq))
    }
  }

  def deletePolygon(id: Int) = AuthAction { implicit request =>
    DB.withConnection { implicit connection =>
      //TODO: check if the authed user is actually allowed to delete this
      SQL"""DELETE FROM polygons WHERE id = $id""".executeUpdate
      Ok("")
    }
  }

}