package controllers

import scala.collection.JavaConversions._
import scala.language.postfixOps
import edu.ithaca.mapdeck.expression.ExpressionParser
import edu.ithaca.mapdeck.expression.ExpressionError
import edu.ithaca.mapdeck.util.Conversions._
import play.api.libs.json.JsValue
import play.api.libs.json.JsString
import play.api.Play.current
import play.api.mvc.Action
import play.api.mvc.Controller
import play.api.libs.json.Json
import web.AuthAction
import play.api.db._
import anorm._
import anorm.SqlParser._
import play.api.libs.json.JsObject
import play.api.libs.json.JsArray

object Expression extends Controller {
  implicit def itr2Seq[T](itr: Iterator[T]): Seq[T] = itr.toSeq

  def inspect(expr: String) = Action { implicit request =>
    def maybePretty(status: Status, json: JsValue) = render {
      case Accepts.Html() => status(Json prettyPrint json)
      case Accepts.Json() => status(json)
    }

    val result = ExpressionParser quickParse expr

    maybePretty(Ok, Json.obj(
      "errors" -> result.getErrors.map(error => Json.obj(
        "message" -> error.getMessage,
        "start" -> error.getStart,
        "length" -> error.getLength
      )),
      "variables" -> result.getVariables.toSeq,
      "functions" -> result.getFunctions.toSeq
    ))
  }

  def evaluate(polygons: Int, expr: String) = AuthAction { implicit request =>
    ExpressionParser.parse(expr).fold({ expression =>
      DB.withConnection { implicit connection =>
        val layerRows = SQL"""SELECT id, name FROM data_layers WHERE set_id = $polygons""".as((for {
          id <- int("id")
          name <- str("name")
        } yield (id, name)).*)

        val layers = Map() ++ (layerRows map {
          case (id, name) =>
            name -> (Map() ++ SQL"""SELECT polygon_id, GROUP_CONCAT(TO_CHAR(val)) AS values, GROUP_CONCAT(TO_CHAR(year)) AS years FROM data_points WHERE layer_id = $id GROUP BY polygon_id ORDER BY polygon_id""".as((for {
              polygon <- int("polygon_id")
              valuesStr <- str("values")
              yearsStr <- str("years")
            } yield (polygon, yearsStr.split(",").toSeq map (_.toInt) zip (valuesStr.split(",").toSeq map (_.toDouble))))*))
        })

        val polygonRows = SQL"""SELECT id FROM polygons WHERE set_id = $polygons""".as((for {
          id <- int("id")
        } yield id).*)

        val years = List.range(2001, 2011) //cheater cheater pumpkin eater

        val result = for (polygon <- polygonRows) yield polygon -> (for (year <- years) yield {
          val bindings = new edu.ithaca.mapdeck.expression.Bindings

          layers foreach {
            case (name, polygons) =>
              val value = polygons(polygon).find(_._1 == year).get._2
              bindings.setVariable(name, value)
          }

          val result = expression.evaluate(bindings)
          year -> result
        })

        val json = new JsObject(result map {
          case (polygon, values) => {
            polygon.toString -> new JsArray(
              values map (t => Json.obj(
                "year" -> t._1,
                "value" -> t._2)
              )
            )
          }
        })

        Ok(json)
      }
    }, { errors =>
      BadRequest("")
    })
  }
}