package controllers

import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import play.api.db._
import play.api.Routes
import play.core.Router.JavascriptReverseRoute
import play.api.Play.current
import org.springframework.security.crypto.bcrypt.BCrypt
import anorm._

object Application extends Controller {

  def index = Action {
    Ok(views.html.Application.index.render)
  }

  case class Credentials(email: String, password: String)

  val loginForm = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText
    )(Credentials.apply)(Credentials.unapply)
  )

  def login = Action {
    Ok(views.html.Application.login.render(loginForm))
  }

  def authenticate = Action { implicit request =>
    val form = loginForm.bindFromRequest
    if (form.hasErrors) {
      BadRequest(views.html.Application.login.render(form))
    } else {
      DB.withConnection { implicit connection =>
        val result = SQL("""SELECT id, password_hash FROM users WHERE email = {email};""").on("email" -> form.get.email)()
        result.headOption.fold {
          BadRequest(views.html.Application.login.render(form.withGlobalError("invalid credentials")))
        } { row =>
          if (BCrypt.checkpw(form.get.password, row[String]("password_hash"))) Redirect(routes.Dashboard.index).withSession("email" -> form.get.email, "id" -> row[Int]("id").toString)
          else BadRequest(views.html.Application.login.render(form.withGlobalError("invalid credentials")))
        }
      }
    }
  }

  def logout = Action {
    Redirect(routes.Application.login).withNewSession
  }

  case class Registration(email: String, password: String, repeat: String)

  val registrationForm = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText,
      "repeat" -> nonEmptyText
    )(Registration.apply)(Registration.unapply) verifying ("passwords must match", fields => fields.password == fields.repeat)
  )

  def register = Action {
    Ok(views.html.Application.register.render(registrationForm))
  }

  def create = Action { implicit request =>
    val form = registrationForm.bindFromRequest
    if (form.hasErrors) {
      BadRequest(views.html.Application.register.render(form))
    } else {
      DB.withConnection { implicit connection =>
        val hash = BCrypt.hashpw(form.get.password, BCrypt.gensalt)
        val result = SQL("INSERT INTO users (email, password_hash) VALUES ({email}, {password_hash});").on("email" -> form.get.email, "password_hash" -> hash).executeInsert()
        result.fold {
          BadRequest(views.html.Application.register.render(form))
        } { id =>
          Redirect(routes.Application.login)
        }
      }
    }
  }

  def todo = TODO
  def todo1(x: AnyRef) = TODO
  def todo2(x: AnyRef, y: AnyRef) = TODO
}