package controllers

import play.api._
import play.api.mvc._
import play.core.Router.JavascriptReverseRoute
import play.twirl.api.Html
import web._

object Dashboard extends Controller {
  val elements = Play.getFile("app/public/elements")(Play.current).listFiles.map(_.getName);
  def index = AuthAction { implicit request =>
    import routes.javascript._

    //Get rid of the "controllers." prefix of the routes
    def shorten(route: JavascriptReverseRoute) = route match {
      case JavascriptReverseRoute(name, f) => JavascriptReverseRoute(name split '.' drop 1 mkString ".", f)
    }

    val jsRoutes = Routes.javascriptRouter("routes")(
      Seq(
        Expression.inspect,
        Expression.evaluate,
        Polygons.listJson,
        Polygons.showJson,
        Data.layers
      ) map shorten: _*
    )

    Ok(views.html.Dashboard.index.render(elements, Html(jsRoutes.toString), request.session.get("email").get))
  }
}
