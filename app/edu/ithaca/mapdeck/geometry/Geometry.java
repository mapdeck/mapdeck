package edu.ithaca.mapdeck.geometry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Geometry {
    String name;
    List<Polygon> polygons;
    
    public Map<String, String> metaData = new HashMap<String, String>();

    public Geometry(String name, List<Polygon> polygons) {
        this.name = name;
        this.polygons = polygons;
    }

    public String getName() {
        return name;
    }

    public List<Polygon> getPolygons() {
        return polygons;
    }
    
    @Override
    public String toString() {
        return "Geometry(" + name + ")";
    }
}
