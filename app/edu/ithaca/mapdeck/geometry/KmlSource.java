package edu.ithaca.mapdeck.geometry;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.micromata.opengis.kml.v_2_2_0.Boundary;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.MultiGeometry;
import de.micromata.opengis.kml.v_2_2_0.Placemark;

public class KmlSource implements GeometrySource {
    Stream<Geometry> geometry;

    public KmlSource(File file) throws FileNotFoundException {
        this(new FileInputStream(file));
    }

    // TODO: I have no idea what real KML files are going to look like and how
    // robust to make this
    public KmlSource(InputStream input) {
        Kml kml = Kml.unmarshal(input);
        Feature top = kml.getFeature();
        geometry = findGeometry(top);
    }

    static Stream<Geometry> findGeometry(Feature feature) {
        if (feature instanceof Document) {
            List<Feature> features = ((Document) feature).getFeature();
            return features.stream().map(KmlSource::findGeometry).reduce(Stream.empty(), Stream::concat);
        } else if (feature instanceof Folder) {
            List<Feature> features = ((Folder) feature).getFeature();
            return features.stream().map(KmlSource::findGeometry).reduce(Stream.empty(), Stream::concat);
        } else if (feature instanceof Placemark) {
            return Stream.of(getGeometry((Placemark)feature));
        } else {
            // TODO maybe should also log somewhere?
            return Stream.empty();
        }
    }

    static Geometry getGeometry(Placemark placemark) {
        //I don't know. Maybe hell could be kind of fun?
        Geometry geom = new Geometry(placemark.getName(), geometryToPolygons(placemark.getGeometry()).collect(Collectors.toList()));
        placemark.getExtendedData().getSchemaData().get(0).getSimpleData().forEach(data -> {
            geom.metaData.put(data.getName(), data.getValue());
        });
        
        return geom;
    }

    static Stream<Polygon> geometryToPolygons(de.micromata.opengis.kml.v_2_2_0.Geometry geometry) {
        if (geometry instanceof MultiGeometry) {
            return ((MultiGeometry) geometry).getGeometry().stream().flatMap(KmlSource::geometryToPolygons);
        } else if (geometry instanceof de.micromata.opengis.kml.v_2_2_0.Polygon) {
            return Stream.of(polygonFromPolygon((de.micromata.opengis.kml.v_2_2_0.Polygon) geometry));
        } else {
            return Stream.empty(); // ignore everything else for now TODO
        }
    }

    static Polygon polygonFromPolygon(de.micromata.opengis.kml.v_2_2_0.Polygon polygon) {
        return new Polygon(pointsFromBoundary(polygon.getOuterBoundaryIs()),
                polygon.getInnerBoundaryIs().stream().map(KmlSource::pointsFromBoundary).collect(Collectors.toList()));
    }

    static List<LatLng> pointsFromBoundary(Boundary boundary) {
        List<Coordinate> coordinates = boundary.getLinearRing().getCoordinates();
        return coordinates.stream().map(coord -> new LatLng(coord.getLatitude(), coord.getLongitude())).collect(Collectors.toList());
    }

    @Override
    public Stream<Geometry> getGeometry() {
        return geometry;
    }

}
