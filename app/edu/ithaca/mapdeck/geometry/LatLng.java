package edu.ithaca.mapdeck.geometry;

public class LatLng {
    double lat;
    double lng;

    public LatLng(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
    
    @Override
    public String toString() {
        //TODO: N/S and E/W
        return Double.toString(lat) + "," + Double.toString(lng);
    }
}
