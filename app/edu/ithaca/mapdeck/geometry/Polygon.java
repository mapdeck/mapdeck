package edu.ithaca.mapdeck.geometry;

import java.util.List;

public class Polygon {
    List<LatLng> outer;
    List<List<LatLng>> inner;
    
    public Polygon(List<LatLng> outer, List<List<LatLng>> inner) {
        this.outer = outer;
        this.inner = inner;
    }

    public List<LatLng> getOuter() {
        return outer;
    }

    public List<List<LatLng>> getInner() {
        return inner;
    }
    
    @Override
    public String toString() {
        return "Polygon(" + outer.toString() + "," + inner.toString() + ")";
    }
}
