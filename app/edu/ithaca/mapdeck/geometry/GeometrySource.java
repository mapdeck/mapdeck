package edu.ithaca.mapdeck.geometry;

import java.util.stream.Stream;

public interface GeometrySource {
    public Stream<Geometry> getGeometry();
}
