package edu.ithaca.mapdeck.expression;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.antlr.v4.runtime.misc.NotNull;

import edu.ithaca.mapdeck.expression.gen.ExpressionBaseVisitor;
import edu.ithaca.mapdeck.expression.gen.ExpressionParser;

public class ExpressionVisitor extends ExpressionBaseVisitor<Expression> {

    @Override
    public Expression visitCall(@NotNull ExpressionParser.CallContext ctx) {
        String name = ctx.ID().getText();

        Function<ExpressionParser.ExprContext, Expression> parse = this::visit;

        List<Expression> arguments = ctx.arguments().expr().stream().map(parse).collect(Collectors.toList());

        return new Call(name, arguments);
    }

    @Override
    public Expression visitInfixOp(@NotNull ExpressionParser.InfixOpContext ctx) {
        List<Expression> args = Arrays.asList(visit(ctx.l), visit(ctx.r));
        return new Call(InfixFunctions.functionMap.get(ctx.op.getText()), args);
    }

    @Override
    public Expression visitNumber(@NotNull ExpressionParser.NumberContext ctx) {
        return new Number(Double.parseDouble(ctx.getText()));
    }

    @Override
    public Expression visitId(@NotNull ExpressionParser.IdContext ctx) {
        return new Variable(ctx.getText());
    }

    @Override
    public Expression visitUnaryOp(@NotNull ExpressionParser.UnaryOpContext ctx) {
        assert ctx.op.getText().equals("-");

        List<Expression> args = Arrays.asList(visit(ctx.expr()));
        JavaFunction negate = new JavaFunction("neg", arg -> -arg.get(0), 1);
        return new Call(negate, args);
    }

    @Override
    public Expression visitParen(@NotNull ExpressionParser.ParenContext ctx) {
        return visit(ctx.expr());
    }
}
