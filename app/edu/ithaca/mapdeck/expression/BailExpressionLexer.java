package edu.ithaca.mapdeck.expression;

import edu.ithaca.mapdeck.expression.gen.ExpressionLexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.LexerNoViableAltException;

public class BailExpressionLexer extends ExpressionLexer {

    public BailExpressionLexer(CharStream input) {
        super(input);
    }

    @Override
    public void recover(LexerNoViableAltException e) {
        throw new RuntimeException(e);
    }
}
