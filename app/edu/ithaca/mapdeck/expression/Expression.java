package edu.ithaca.mapdeck.expression;

public interface Expression {

    public double evaluate(Bindings bindings);
}
