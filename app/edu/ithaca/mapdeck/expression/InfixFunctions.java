package edu.ithaca.mapdeck.expression;

import java.util.HashMap;
import java.util.Map;

public class InfixFunctions {

    public static JavaFunction multiply = new JavaFunction("mul", args -> args.get(0) *
                                                                          args.get(1), 2);
    public static JavaFunction divide = new JavaFunction("div", args -> args.get(0) /
                                                                        args.get(1), 2);
    public static JavaFunction add = new JavaFunction("add", args -> args.get(0) +
                                                                     args.get(1), 2);
    public static JavaFunction subtract = new JavaFunction("sub", args -> args.get(0) -
                                                                          args.get(1), 2);
    public static JavaFunction modulo = new JavaFunction("mod", args -> args.get(0) %
                                                                        args.get(1), 2);
    public static JavaFunction power = new JavaFunction("pow", args -> Math.pow(args.get(0), args.get(1)), 2);

    public static Map<String, JavaFunction> functionMap = new HashMap<String, JavaFunction>();

    static {
        functionMap.put("*", multiply);
        functionMap.put("/", divide);
        functionMap.put("+", add);
        functionMap.put("-", subtract);
        functionMap.put("%", modulo);
        functionMap.put("^", power);
    }
}
