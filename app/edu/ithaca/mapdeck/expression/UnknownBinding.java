package edu.ithaca.mapdeck.expression;

public class UnknownBinding extends RuntimeException {

    private static final long serialVersionUID = 24L;

    String variable;

    public UnknownBinding(String variable) {
        super(variable);
        this.variable = variable;
    }
}
