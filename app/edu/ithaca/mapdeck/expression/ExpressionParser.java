package edu.ithaca.mapdeck.expression;

import java.util.Collection;

import org.antlr.v4.runtime.ANTLRErrorListener;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BufferedTokenStream;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import edu.ithaca.mapdeck.expression.gen.*;

import edu.ithaca.mapdeck.util.Either;

public class ExpressionParser {
    static ParseTree parseTree(String input, ANTLRErrorListener errorListener) {
        CharStream charStream = new ANTLRInputStream(input);
        ExpressionLexer lexer = new ExpressionLexer(charStream);

        lexer.removeErrorListeners();
        lexer.addErrorListener(errorListener);

        TokenStream tokens = new BufferedTokenStream(lexer);
        edu.ithaca.mapdeck.expression.gen.ExpressionParser parser = new edu.ithaca.mapdeck.expression.gen.ExpressionParser(
                tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(errorListener);

        return parser.expr();
    }
    
    public static InspectionResult quickParse(String input) {
        ExpressionErrorListener errorListener = new ExpressionErrorListener();
        ParseTree tree = parseTree(input, errorListener);
        
        ParseTreeWalker walker = new ParseTreeWalker();
        SymbolCollector collector = new SymbolCollector();
        walker.walk(collector, tree);
        
        return new InspectionResult(errorListener.getErrors(), collector.getVariables(), collector.getFunctions());
    }

    public static Either<Expression, Collection<ExpressionError>> parse(String input) {
        ExpressionErrorListener errorListener = new ExpressionErrorListener();
        ParseTree tree = parseTree(input, errorListener);
        
        if (errorListener.anyError()) {
            return Either.right(errorListener.getErrors());
        } else {
            ExpressionVisitor visitor = new ExpressionVisitor();
            Expression expression = visitor.visit(tree);

            return Either.left(expression);
        }
    }
}
