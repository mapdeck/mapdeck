package edu.ithaca.mapdeck.expression;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import edu.ithaca.mapdeck.util.Either;

public class Call implements Expression {

    Either<ExpressionFunction, String> fn;
    List<Expression> args;

    public Call(ExpressionFunction fn, List<Expression> args) {
        this.fn = Either.left(fn);
        this.args = args;
    }

    public Call(String name, List<Expression> args) {
        this.fn = Either.right(name);
        this.args = args;
    }

    @Override
    public double evaluate(Bindings bindings) {
        Consumer<Integer> checkArity = arity -> {
            if(arity != args.size()) throw new RuntimeException("The singing wasn't great, girls");
        };
        
        return fn.map(fn -> fn.call(this.args.stream().map(arg -> arg.evaluate(bindings)).collect(Collectors.toList())),
                      name -> {
                          ExpressionFunction fn = bindings.getFunction(name);
                          fn.getArity().ifPresent(checkArity);
                          return fn.call(this.args.stream().map(arg -> arg.evaluate(bindings)).collect(Collectors.toList()));
                      });
    }

    @Override
    public String toString() {
        List<String> argStrings = args.stream().map(exp -> exp.toString()).collect(Collectors.toList());
        return fn.map(f -> f.getName(), s -> s) + "(" +
               StringUtils.join(argStrings, ", ") + ")";
    }
}
