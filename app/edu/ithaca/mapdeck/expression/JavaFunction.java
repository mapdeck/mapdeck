package edu.ithaca.mapdeck.expression;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

public class JavaFunction implements ExpressionFunction {

    Function<List<Double>, Double> impl;
    Optional<Integer> arity;
    String name;

    public JavaFunction(String name, Function<List<Double>, Double> impl,
            int arity) {
        this(name, impl, Optional.of(arity));
    }

    public JavaFunction(String name, Function<List<Double>, Double> impl,
            Optional<Integer> arity) {
        this.name = name;
        this.impl = impl;
        this.arity = arity;
    }

    public double call(List<Double> args) {
        arity.ifPresent(n -> {
            assert args.size() == n;
        });

        return impl.apply(args);
    }

    public Optional<Integer> getArity() {
        return arity;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
