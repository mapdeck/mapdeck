package edu.ithaca.mapdeck.expression;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.misc.Interval;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.misc.Nullable;

public class ExpressionErrorListener extends BaseErrorListener {

    List<ExpressionError> errors;

    public ExpressionErrorListener() {
        errors = new LinkedList<ExpressionError>();
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        try {
            int length = Optional.ofNullable(offendingSymbol).map(o -> {
                Token token = ((Token) o);
                return token.getType() == Token.EOF ? 0 : token.getText().length();
            }).orElseGet(() -> {
                // Haha
                Lexer lexer = (Lexer) recognizer;
                int start = lexer._tokenStartCharIndex;
                CharStream input = (CharStream) lexer._input;
                return input.index() - start + 1;
            });

            errors.add(new ExpressionError(msg, charPositionInLine, length));
        } catch (Exception ex) {
            errors.add(new ExpressionError("Internal error: " + ex.getMessage(), 0, 1));
        }
    }

    public boolean anyError() {
        return errors.size() > 0;
    }

    public Collection<ExpressionError> getErrors() {
        return errors;
    }
}
