package edu.ithaca.mapdeck.expression;

import java.util.Collection;

import scala.Tuple3;

public class InspectionResult {
    Collection<ExpressionError> errors;
    Collection<String> variables;
    Collection<String> functions;

    public InspectionResult(Collection<ExpressionError> errors,
            Collection<String> variables, Collection<String> functions) {
        this.errors = errors;
        this.variables = variables;
        this.functions = functions;
    }

    public Collection<ExpressionError> getErrors() {
        return errors;
    }

    public void setErrors(Collection<ExpressionError> errors) {
        this.errors = errors;
    }

    public Collection<String> getVariables() {
        return variables;
    }

    public void setVariables(Collection<String> variables) {
        this.variables = variables;
    }

    public Collection<String> getFunctions() {
        return functions;
    }

    public void setFunctions(Collection<String> functions) {
        this.functions = functions;
    }
}