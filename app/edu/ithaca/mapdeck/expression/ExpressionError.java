package edu.ithaca.mapdeck.expression;

public class ExpressionError {
    String message;
    int start;
    int length;

    public ExpressionError(String message, int start, int length) {
        this.message = message;
        this.start = start;
        this.length = length;
    }
    
    public String getMessage() {
        return message;
    }

    public int getStart() {
        return start;
    }

    public int getLength() {
        return length;
    }
}
