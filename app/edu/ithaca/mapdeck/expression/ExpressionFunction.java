package edu.ithaca.mapdeck.expression;

import java.util.List;
import java.util.Optional;

public interface ExpressionFunction {

    double call(List<Double> args);

    Optional<Integer> getArity();

    String getName();
}
