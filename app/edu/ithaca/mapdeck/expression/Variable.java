package edu.ithaca.mapdeck.expression;

public class Variable implements Expression {

    String name;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public double evaluate(Bindings bindings) throws UnknownBinding {
        return bindings.getVariable(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
