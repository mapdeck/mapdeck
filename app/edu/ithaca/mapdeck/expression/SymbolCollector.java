package edu.ithaca.mapdeck.expression;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import edu.ithaca.mapdeck.expression.gen.ExpressionBaseListener;
import edu.ithaca.mapdeck.expression.gen.ExpressionParser.CallContext;
import edu.ithaca.mapdeck.expression.gen.ExpressionParser.IdContext;


public class SymbolCollector extends ExpressionBaseListener {
    Set<String> variables = new HashSet<String>();
    Set<String> functions = new HashSet<String>();
    
    @Override
    public void enterCall(CallContext ctx) {
        functions.add(ctx.ID().getText());
    }

    @Override
    public void enterId(IdContext ctx) {
        variables.add(ctx.getText());
    }

    public Collection<String> getVariables() {
        return variables;
    }
    
    public Collection<String> getFunctions() {
        return functions;
    }
}
