package edu.ithaca.mapdeck.expression;

public class Number implements Expression {

    double number;

    public Number(double number) {
        this.number = number;
    }

    @Override
    public double evaluate(Bindings bindings) {
        return number;
    }

    @Override
    public String toString() {
        return Double.toString(number);
    }
}
