package edu.ithaca.mapdeck.expression;

import java.util.HashMap;

public class Bindings {

    Bindings parent;

    HashMap<String, Double> variables = new HashMap<String, Double>();
    HashMap<String, ExpressionFunction> functions = new HashMap<String, ExpressionFunction>();

    public Bindings() {
        this(basic);
    }

    public Bindings(Bindings parent) {
        this.parent = parent;
    }

    public double getVariable(String name) throws UnknownBinding {
        if(!variables.containsKey(name)) return getParentVariable(name);
        return variables.get(name);
    }

    public void setVariable(String name, double value) {
        variables.put(name, value);
    }

    public void removeVariable(String name) {
        variables.remove(name);
    }

    double getParentVariable(String name) throws UnknownBinding {
        if(parent == null) throw new UnknownBinding(name);
        return parent.getVariable(name);
    }

    public ExpressionFunction getFunction(String name) throws UnknownBinding {
        if(!functions.containsKey(name)) return getParentFunction(name);
        return functions.get(name);
    }

    public void setFunction(String name, ExpressionFunction fn) {
        functions.put(name, fn);
    }

    public void removeFunction(String name) {
        functions.remove(name);
    }

    ExpressionFunction getParentFunction(String name) throws UnknownBinding {
        if(parent == null) throw new UnknownBinding(name);
        return parent.getFunction(name);
    }

    public static Bindings empty() {
        return new Bindings(null);
    }

    static Bindings constants = new Bindings(null);

    static {
        constants.setVariable("PI", Math.PI);
        constants.setVariable("E", Math.E);
    }

    static Bindings basic = new Bindings(constants);

    static {
        basic.setFunction("abs",
                          new JavaFunction("abs", args -> Math.abs(args.get(0)), 1));
    }
}
