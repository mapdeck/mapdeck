package edu.ithaca.mapdeck.util;

import java.util.Optional;

public class Right<A, B> extends Either<A, B> {

    public Right(B value) {
        right = Optional.of(value);
    }

    @Override
    public boolean isLeft() {
        return false;
    }
}
