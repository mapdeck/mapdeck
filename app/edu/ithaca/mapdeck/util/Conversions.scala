package edu.ithaca.mapdeck.util

import scala.language.implicitConversions
import scala.{Left => ScalaLeft, Right => ScalaRight, Either => ScalaEither}
import edu.ithaca.mapdeck.util.{Either => JavaEither, Left => JavaLeft, Right => JavaRight}
import java.util.{ Optional => JavaOption }

object Conversions {
  implicit def either2Either[A, B](either: JavaEither[A, B]): ScalaEither[A, B] = {
    either match {
      case left: JavaLeft[A, B] => ScalaLeft(left.getLeft)
      case right: JavaRight[A, B] => ScalaRight(right.getRight)
    }
  }
  
  implicit def optional2Option[T](option: JavaOption[T]): Option[T] = if(option.isPresent) Option(option.get) else Option.empty
}