package edu.ithaca.mapdeck.util;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public abstract class Either<A, B> {

    protected Optional<A> left = Optional.empty();
    protected Optional<B> right = Optional.empty();

    public abstract boolean isLeft();

    public <C> C map(Function<? super A, ? extends C> lMap, Function<? super B, ? extends C> rMap) {
        if(isLeft()) return lMap.apply(getLeft());
        else return rMap.apply(getRight());
    }

    public void ifLeft(Consumer<? super A> consumer) {
        left.ifPresent(l -> consumer.accept(l));
    }

    public void ifRight(Consumer<? super B> consumer) {
        right.ifPresent(r -> consumer.accept(r));
    }

    public A getLeft() throws NoSuchElementException {
        if(!isLeft()) throw new NoSuchElementException();
        return left.get();
    }

    public B getRight() throws NoSuchElementException {
        if(isLeft()) throw new NoSuchElementException();
        return right.get();
    }

    public A leftOrElse() {
        return null;
    }

    public B rightOrElse() {
        return null;
    }

    public static <A, B> Either<A, B> left(A a) {
        return new Left<A, B>(a);
    }

    public static <A, B> Either<A, B> right(B b) {
        return new Right<A, B>(b);
    }
}
