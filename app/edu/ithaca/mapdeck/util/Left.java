package edu.ithaca.mapdeck.util;

import java.util.Optional;

public class Left<A, B> extends Either<A, B> {

    public Left(A value) {
        left = Optional.of(value);
    }

    @Override
    public boolean isLeft() {
        return true;
    }
}
