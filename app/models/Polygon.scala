package models

import java.sql.Connection
import anorm._
import anorm.SqlParser._

//TODO: right now it would be easy to get false information on an object if you change the underlying record,
//but I don't want to do a database check for every single property access. This whole thing is messy. Think about
//it more before adding too much to this file

/**
 * Simple ORM-style things for working with the database.
 * They shouldn't be used after the database connection
 * is closed or bad things will happen.
 */
class PolygonSet(_id: Int, _name: String, _ownerId: Int, _ready: Boolean)(implicit connection: Connection) {
  def id = _id
  def name = _name
  def ownerId = _ownerId
  def ready = _ready

  def polygons = {
    val setId = id
    SQL"""SELECT * FROM polygons WHERE set_id = $id""".as((
      for {
        id <- int("id")
        name <- str("name")
        json <- str("geometry_json")
      } yield new Polygon(id, setId, name, json)
    )*)
  }
}

object PolygonSet {
  def apply(id: Int)(implicit connection: Connection): Option[PolygonSet] = {
    SQL"""SELECT * FROM polygon_sets WHERE id = $id""".as((
      for {
        name <- str("name")
        ownerId <- int("owner_id")
        ready <- bool("ready")
      } yield new PolygonSet(id, name, ownerId, ready)
    ).singleOpt)
  }
}

class Polygon(_id: Int, _setId: Int, _name: String, _json: String)(implicit connection: Connection) {
  def id = _id
  def setId = _setId
  def name = _name
  def json = _json
}

object Polygon {
  def apply(id: Int)(implicit connection: Connection): Option[Polygon] = {
    val result = SQL"""SELECT * FROM polygons WHERE id = $id"""()

    result.headOption.fold[Option[Polygon]] {
      None
    } { row =>
      val set_id = row[Int]("set_id")
      val name = row[String]("name")
      val geometry_json = row[String]("geometry_json")

      val polygon = new Polygon(id, set_id, name, geometry_json)

      Some(polygon)
    }
  }
}