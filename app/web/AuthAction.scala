package web

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import controllers.routes

case class Auth[A](action: Action[A]) extends Action[A] {

  def apply(request: Request[A]): Future[Result] = {
    //TODO: IMPORTANT!!! check if request.secure once we start using SSL
    request.session.get("email").fold {
      //TODO: check database lol
      Future[Result](Results.Redirect(routes.Application.login)) //TODO: probably shouldn't hardcode this redirect, or at least the route
    } { email =>
      action(request)
    }
  }

  lazy val parser = action.parser
}

object AuthAction extends ActionBuilder[AuthRequest] {
  def invokeBlock[A](request: Request[A], block: (AuthRequest[A]) => Future[Result]) = {
    //Hmm, I don't think there's a way to share this code with Auth's apply()... so okay.
    //Error checking isn't important here since we know the Auth action passed, but that 
    //doesn't make me feel warm and fuzzy inside.
    block(AuthRequest(request, new User(Integer.parseInt(request.session.get("id").get), request.session.get("email").get)))
  }

  override def composeAction[A](action: Action[A]) = new Auth(action)
}

/**
 * An authenticated request with user information
 */
case class AuthRequest[A](request: Request[A], user: User) extends WrappedRequest[A](request)

class User(_id: Int, _email: String) {
  def id = _id
  def email = _email

  
  def this(id: Int) = {
    //TODO: user a more descriptive exception type

    //Throw an exception if the user doesn't exist (or all the time for now)
    this(0, "")
    throw new Exception()
  }
}

object User {
  /**
   * Look up the rest of the user info in the database based on the id
   */
  def lookup(id: Int) = Option(new User(0, ""))
}